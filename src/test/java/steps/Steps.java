/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class Steps {
	public ChromeDriver driver;
	@And("Open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@And("Max the browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	    
	}

	@And("set the timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  
	}

	@And("Launch the browser")
	public void launchTheBrowser() {
		driver.get("http://leaftaps.com/opentaps/");
	    
	}

	@And("Enter the username as (.*)")
	public void enterTheUsername(String data) {
		driver.findElementById("username").sendKeys(data);
	    
	}

	@And("Enter the password as (.*)")
	public void enterThePassword(String data) {
		driver.findElementById("password").sendKeys(data);
	    
	    
	}

	@And("click on the Login Button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	@And("click CRM\\/SFA")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	    
	}

	@And("click Leads")
	public void clickLeads() {
		driver.findElementByLinkText("Create Lead").click();
	    
	}

	@And("click Create Lead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	    
	}

	@And("Enter the companyname")
	public void enterTheCompanyname() {
		driver.findElementById("createLeadForm_companyName").sendKeys("Amazon");
	    
	}

	@And("Enter the firstName")
	public void enterTheFirstName() {
		driver.findElementById("createLeadForm_firstName").sendKeys("Prashanth");
	    
	    
	}

	@And("Enter the lastName")
	public void enterTheLastName() {
		driver.findElementById("createLeadForm_lastName").sendKeys("Rameshbabu");
	    
	}

	@When("click Submit")
	public void clickSubmit() {
		driver.findElementByName("submitButton").click();
	    
	}

	@Then("Verify new lead is created")
	public void verifyNewLeadIsCreated() {
	    
	    
	}

	

}
*/