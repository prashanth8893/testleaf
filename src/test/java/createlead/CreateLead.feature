Feature: Create a Lead

Background:
Given Open the browser
And Max the browser
And set the timeout
And Launch the browser


Scenario Outline: Add a Lead

And Enter the username as <username>
And Enter the password as <password>
And click on the Login Button

Examples:
|username||password|
|DemoSalesManager||crmsfa|
|DemoCSR||crmsfa|


And click CRM/SFA
And click Leads
And click Create Lead
And Enter the companyname
And Enter the firstName
And Enter the lastName
When click Submit
Then Verify new lead is created

#Scenario: Add a Lead
#
#And Enter the username
#And Enter the password
#And click on the Login Button
#And click CRM/SFA
#And click Leads
#And click Create Lead
#And Enter the companyname
#And Enter the firstName
#And Enter the lastName
#When click Submit
#Then Verify new lead is created
