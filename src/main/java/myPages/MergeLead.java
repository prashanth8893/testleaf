package myPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{
	public MergeLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="From Lead") WebElement eleFromLead;
	@FindBy(id="To Lead") WebElement eleToLead;
	@FindBy(linkText="Merge") WebElement eleMerge;
	public MergeLead FromLead(String fromlead)
	{
		//SendKeysAction(eleFromLead, fromlead);
		type(eleFromLead, fromlead);
		return this;
}
	public MergeLead ToLead(String tolead)
	{
		//SendKeysAction(eleToLead, tolead);
		type(eleToLead, tolead);
		return this;
}
	public MergeLead ClickMerge()
	{
		click(eleMerge);
		return this;
}
	
}