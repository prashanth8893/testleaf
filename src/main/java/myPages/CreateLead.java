package myPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName") WebElement eleCompany;
	@FindBy(id="createLeadForm_firstName") WebElement eleFirstname;
	@FindBy(id="createLeadForm_lastName") WebElement eleLastname;
	@FindBy(name="submitButton") WebElement eleSubmit;
	public CreateLead CompanyName(String companyname)
	{
		type(eleCompany, companyname);
		return this;
	}
	public CreateLead FirstName(String firstName)
	{
		type(eleFirstname, firstName);
		return this;
}
	public CreateLead LastName(String lastName)
	{
		type(eleLastname, lastName);
		return this;
	}
	public ViewLead Submit()
	{
		click(eleSubmit);
		return new ViewLead();	
}
}