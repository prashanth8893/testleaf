package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import myPages.LoginPage;
import utils.DataInputProvider;
import wdMethods.ProjectMethods;

public class CreatLead {
	
	public class TC001_LoginAndLogout extends ProjectMethods{
		@BeforeTest
		public void setData() {
			testCaseName = "TC001_LoginAndLogout";
			testDescription = "LoginAndLogout";
			authors = "sarath";
			category = "smoke";
			dataSheetName = "datapro";
			testNodes = "Leads";
		}
		
		@Test(dataProvider="fetchData")
		public void creatl(String username, String password, String companyname, String firstName, String lastName)
		{
			new LoginPage()
			.enterUserName(username)
			.enterPassword(password)
			.clickLogin()
			.ClickCrm()
			.ClickLeads()
			.ClickCrleads()
			.CompanyName(companyname)
			.FirstName(firstName)
			.LastName(lastName)
			.Submit()
			.viewLead();
		}
		
		@DataProvider(name="fetchData")
		public  Object[][] getData(){
			return DataInputProvider.getSheet(dataSheetName);		
		}	

	}}
